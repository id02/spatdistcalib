#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:30:11 2022

@author: opid02
"""
import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

#import fabio, os
#import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt
#import scipy.interpolate as ipol
import scipy.sparse as sp
#import matplotlib as mpl

logger = logging.getLogger(__name__)

def spline_correction(data, splx, sply):
    
    logger.info("Generate displacement matrix from spline")
    x = np.arange(data.shape[1])
    y = np.arange(data.shape[0])
    
    X,Y = np.meshgrid(x,y)
    
    Xc = X + splx(x,y).T
    Yc = Y + sply(x,y).T
    
    Xc00 = np.floor(Xc)
    Yc00 = np.floor(Yc)
    
    logger.info("Transformation matrix calculation")
    k00 = (1-(Xc - Xc00))*(1-(Yc-Yc00))
    k10 = (Xc - Xc00)*(1-(Yc-Yc00))
    k01 = (1-(Xc - Xc00))*(Yc-Yc00)
    k11 = (Xc - Xc00)*(Yc-Yc00)
    
    trm = sp.lil_matrix((data.shape[0]*data.shape[1], data.shape[0]*data.shape[1]))
    
    logger.info("... Masking")
    msk0x = np.logical_and(Xc00 >= 0, Xc00 < data.shape[1])
    msk0y = np.logical_and(Yc00 >= 0, Yc00 < data.shape[0])    
    msk1x = np.logical_and(Xc00+1 >= 0, Xc00+1 < data.shape[1])
    msk1y = np.logical_and(Yc00+1 >= 0, Yc00+1 < data.shape[0])
    
    msk00  = np.logical_and(msk0x, msk0y).ravel()
    msk11  = np.logical_and(msk1x, msk1y).ravel()
    msk10  = np.logical_and(msk1x, msk0y).ravel()
    msk01  = np.logical_and(msk0x, msk1y).ravel()
    
    logger.info("... Indexing")
    Idx   = (Y*data.shape[1] + X).ravel()
    Idx00 = (Yc00*data.shape[1] + Xc00).ravel()
    Idx10 = (Yc00*data.shape[1] + Xc00 + 1).ravel()
    Idx01 = ((Yc00+1)*data.shape[1] + Xc00).ravel()
    Idx11 = ((Yc00+1)*data.shape[1] + Xc00 + 1).ravel()
    
    logger.info("... Summation")
    trm[Idx[msk00],Idx00[msk00]] += k00.ravel()[msk00]
    trm[Idx[msk10],Idx10[msk10]] += k10.ravel()[msk10]
    trm[Idx[msk01],Idx01[msk01]] += k01.ravel()[msk01]
    trm[Idx[msk11],Idx11[msk11]] += k11.ravel()[msk11]
    
    logger.info("... Normalization")
    k = (1/np.sum(trm, axis=0)).A.ravel()    
    sk = sp.diags(k)
    trm *= sk
    
    logger.info("Image correction")
    D = data.ravel()*trm
    D.shape = data.shape
    
    return D

def find_peak_fit(data, center, radius, ax=None):
    
    def fun(XY, k, x0, y0, r, A):
        X = XY[0]
        Y = XY[1]
        
        return (A*(0.5-0.5*np.tanh(k*(((X-x0)**2+(Y-y0)**2)**0.5-r))))
    
    radius = int(radius)
    center = (int(center[0]), int(center[1]))
    
    x = np.arange(center[0]-radius, center[0]+radius)
    y = np.arange(center[1]-radius, center[1]+radius)
    
    subm = data[y[0]:y[-1]+1,x[0]:x[-1]+1]
    subm -= np.min(subm[:])
    X,Y = np.meshgrid(x,y)
    
    logger.debug(X.shape, Y.shape, subm.shape)
    
    p0 = [10,center[0],center[1],3,np.max(subm[:])]
    k = opt.curve_fit(lambda *args: fun(*args).ravel(), [X,Y,], subm.ravel(),p0)
    
    if ax is not None:
        ax.scatter(X,Y,subm)
        #ax.scatter(X,Y,fun([X,Y,],*k[0]))
        ax.plot_surface(X,Y,fun([X,Y,],*k[0]))
        
    return (k[0][1], k[0][2])

def find_peak_bari(data, center, radius, ax=None):
    radius = int(radius)
    center = (int(center[0]), int(center[1]))
    
    x = np.arange(center[0]-radius, center[0]+radius)
    y = np.arange(center[1]-radius, center[1]+radius)
    
    subm = data[y[0]:y[-1]+1,x[0]:x[-1]+1]
    
    X,Y = np.meshgrid(x,y)
    
    threshold = 0.6
    
    M = np.max(subm[:])
    m = np.min(subm[:])
    
    msk = subm > threshold*(M-m)+m
    
    std = np.std(subm[True ^ msk])
    
    if M < m + 3*std**0.5 or np.all(msk) or np.all(True ^ msk):
        raise ValueError("No peak found")
    
    S = np.sum(subm[msk])
    res = (np.sum(X[msk]*subm[msk])/S, np.sum(Y[msk]*subm[msk])/S)
    
    if ax is not None:
        ax.scatter(X,Y,subm)
        ax.scatter(X[msk],Y[msk],subm[msk])
        ax.scatter(res[0],res[1],np.max(subm)*1.1)
   
    
    return res
    

def find_peak(data,*args):
    return find_peak_bari(data, *args)