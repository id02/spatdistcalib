#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:34:55 2022

@author: opid02
"""

import numpy as np

def write_array(fd, arr):
    for i,a in enumerate(arr):
        if i > 0 and not np.mod(i,5):
            fd.write('\n')
        if a < 0:
            fd.write('%.7E'%a)
        else:
            fd.write(' %.7E'%a)
    fd.write('\n')
    
    
def write_spline_file(oname, splx, sply, px, py, sh):
    with open(oname,'w') as fd:
        fd.write('SPATIAL DISTORTION SPLINE INTERPOLATION COEFFICIENTS\n\n')
        fd.write('  BINNING\n')
        fd.write(' 1.0000000E+00 1.0000000E+00\n\n')
        fd.write('  VALID REGION\n')
        fd.write(' 0.0000000E+00 0.0000000E+00 %.7E %.7E\n\n'%(sh[1], sh[0]))
        fd.write('  GRID SPACING, X-PIXEL SIZE, Y-PIXEL SIZE\n')
        fd.write(' %.7E %.7E %.7E\n\n'%(500,px*1e3, py*1e3))
        fd.write('\n')
        fd.write('  X-DISTORTION\n')
        fd.write(' {:5d} {:5d}\n'.format(len(splx.get_knots()[0]), len(splx.get_knots()[1])))
        write_array(fd, splx.get_knots()[0])
        write_array(fd, splx.get_knots()[1])
        write_array(fd, splx.get_coeffs())
        fd.write('  Y-DISTORTION\n')
        fd.write(' {:5d} {:5d}\n'.format(len(sply.get_knots()[0]), len(sply.get_knots()[1])))
        write_array(fd, sply.get_knots()[0])
        write_array(fd, sply.get_knots()[1])
        write_array(fd, sply.get_coeffs())
            