#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 11:43:09 2022

@author: opid02
"""

import fabio
from PIL import Image
import sys, numpy as np


def main():

    imname = sys.argv[1]
    outname = "%s.edf"%imname.split('/')[-1]
    
    with Image.open(sys.argv[1]) as pic:
        data = 255-0.8*np.array(pic)
        
        edfIm = fabio.edfimage.EdfImage(data=data)
        edfIm.save(outname)
    