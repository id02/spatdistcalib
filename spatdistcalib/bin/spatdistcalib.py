#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 11:52:28 2022

@author: opid02
"""

import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

import fabio, os
import matplotlib.pyplot as plt
import numpy as np
import scipy.interpolate as ipol
import matplotlib as mpl

logger = logging.getLogger(__name__)
plt.ion()

from ..utils import find_peak, spline_correction, write_spline_file

def process(file, radius, output_file, psize_cor=True):
    logger.info("Processing file %s"%file)
    
    with fabio.open(file) as f:
        data = f.data
        
    plt.figure(1)
    plt.clf()
    plt.imshow(np.log(data), origin='lower')
    plt.axis('scaled')
    
    # Get origin coordinates from user input
    print("Select origin and 2 points along the main axes from the origin. Right click remove the last point.")
    coords = plt.ginput(3,0,True)
    
    # From user coordinates, find better center for the initial vectors
    O = find_peak(data, coords[0], radius)
    Oe1 = find_peak(data, coords[1], radius)
    Oe2 = find_peak(data, coords[2], radius)
    
    e1 = (Oe1[0] - O[0], Oe1[1] - O[1])
    e2 = (Oe2[0] - O[0], Oe2[1] - O[1])
    
    plt.figure(1)
    
    # Probe along the main directions to get finer main vectors    
    logger.info("Probing along the main directions")
    X1e1 = []
    Y1e1 = []
    x = O[0]
    y = O[1]
    
    while x > radius and x < data.shape[1]-radius and y > radius and y < data.shape[0]-radius:  
        try:
            A = find_peak(data, (x,y), radius)
        except ValueError:
            break
        
        X1e1 += [A[0],]
        Y1e1 += [A[1],]
        
        x = A[0] + e1[0]
        y = A[1] + e1[1]
        
    X2e1 = []
    Y2e1 = []
    x = O[0] - e1[0]
    y = O[1] - e1[1]
    
    while x > radius and x < data.shape[1]-radius and y > radius and y < data.shape[0]-radius:        
        try:
            A = find_peak(data, (x,y), radius)
        except ValueError:
            break
        
        X2e1 += [A[0],]
        Y2e1 += [A[1],]
        
        x = A[0] - e1[0]
        y = A[1] - e1[1]
        
    X1e2 = []
    Y1e2 = []
    x = O[0]
    y = O[1]
    
    while x > radius and x < data.shape[1]-radius and y > radius and y < data.shape[0]-radius:        
        try:
            A = find_peak(data, (x,y), radius)
        except ValueError:
            break
                
        X1e2 += [A[0],]
        Y1e2 += [A[1],]
        
        x = A[0] + e2[0]
        y = A[1] + e2[1]
        
    X2e2 = []
    Y2e2 = []
    x = O[0]-e2[0]
    y = O[1]-e2[1]
    
    while x > radius and x < data.shape[1]-radius and y > radius and y < data.shape[0]-radius:        
        try:
             A = find_peak(data, (x,y), radius)
        except ValueError:
            break
        
        X2e2 += [A[0],]
        Y2e2 += [A[1],]
        
        x = A[0] - e2[0]
        y = A[1] - e2[1]
        
    X2e1.reverse()
    Y2e1.reverse()
    X2e2.reverse()
    Y2e2.reverse()
        
    cross_e1 = np.array([[*X2e1,*X1e1],[*Y2e1,*Y1e1]]).T
    cross_e2 = np.array([[*X2e2,*X1e2],[*Y2e2,*Y1e2]]).T
    
#    logger.debug(cross_e1)
#    logger.debug(cross_e2)
    
    plt.plot(cross_e1[:,0], cross_e1[:,1], 'gx-', label='Main axes')
    plt.plot(cross_e2[:,0], cross_e2[:,1], 'gx-')
    
    e1 = np.mean(np.diff(cross_e1, axis=0), axis=0)
    e2 = np.mean(np.diff(cross_e2, axis=0), axis=0)
    
    # Generate the list of coordinates
    logger.info("Generating regular grid")
    L = (data.shape[0]**2 + data.shape[1]**2)**0.5
    l = min((e1[0]**2 + e1[1]**2)**0.5, (e2[0]**2 + e2[1]**2)**0.5)
    N = np.ceil(L/l)
    
#    logger.debug(e1)
#    logger.debug(e2)
#    logger.debug(data.shape)
#    logger.debug(N)
#    logger.debug(L)
#    logger.debug(l)
    
    ii = np.arange(-N,N)
    I,J = np.meshgrid(ii,ii)
    
    X = O[0] + I*e1[0] + J*e2[0]
    Y = O[1] + I*e1[1] + J*e2[1]

    mskx = np.logical_and(X >= radius, X < data.shape[1]-radius)
    msky = np.logical_and(Y >= radius, Y < data.shape[0]-radius)
    mskxy = np.logical_and(mskx, msky)
    
    XY = np.array([[*X[mskxy]],[*Y[mskxy]]]).T
    
    plt.plot(XY[:,0], XY[:,1],'.b', label="Regular grid")       
    
    # Fine search around all determined coordinates
    logger.info("Find each individual peaks")
    XY_fine = np.empty(XY.shape)
    to_delete = []
    
    for i in range(XY.shape[0]):
        try:
            A = find_peak(data, XY[i,:],radius)
            XY_fine[i,:] = A
            
            if not np.mod(i, 100) and i > 0:
                logger.info("Found %i peaks"%i)
        except ValueError:
            to_delete += [i,]
        
    plt.plot(XY_fine[:,0], XY_fine[:,1], '.r', label="Actual grid")

    displ = XY - XY_fine

    # Remove anomalities
    logger.info("Removing displacement anomalities")
    coer_radius = 3*(e1[0]**2 + e1[1]**2 + e2[0]**2 + e2[1]**2)**0.5
    for i in range(XY_fine.shape[0]):
        msk = ((XY_fine[:,0] - XY_fine[i,0])**2 + (XY_fine[:,1] - XY_fine[i,1])**2)**0.5 < coer_radius
        
        D = np.mean(displ[msk,:], axis=0)
        Ds = np.std(displ[msk,:], axis=0)
        
        if displ[i,0] > D[0] + 3*Ds[0] or displ[i,0] < D[0] - 3*Ds[0] or displ[i,1] > D[1] + 3*Ds[1] or displ[i,1] < D[1] - 3*Ds[1]:
            logger.debug("Anomality at %s, removing"%XY_fine[i,:])
            to_delete += [i,]
            
    logger.info("%i anomalities found"%len(to_delete))
            
    plt.plot(XY_fine[to_delete,0], XY_fine[to_delete, 1], 'yx', label="Removed grid points")
            
    displ = np.delete(displ, to_delete, axis=0)
    XY = np.delete(XY, to_delete, axis=0)
    XY_fine = np.delete(XY_fine, to_delete, axis=0)
    
    plt.legend(loc='center left', bbox_to_anchor=(1.04,0.5))
    
    # Splines computations on regular grid
    
    logger.info("Calculating pixel size") 
    doagain = True
    Theta = np.round(np.arccos(e1.dot(e2)/(e1.dot(e1)*e2.dot(e2))**.5)*36/np.pi)*5
    
    while doagain:
        d1 = input("\nReal space distance in e1 direction in mm: ")
        d2 = input("Real space distance in e2 direction in mm: ")
        theta = input("Angle between main directions [%.2f]: "%Theta)
        try:
            d1 = float(d1)
            d2 = float(d2)
            if theta != '':
                Theta = float(theta)
            
        except Exception as e:
            print(e)
            continue
        
        doagain = False
        
    d3_2 = d1**2 + d2**2 - 2*d1*d2*np.cos(Theta*np.pi/180.)
    e3 = e1 - e2
    
    A1 = np.array([e1**2,e2**2])
    A2 = np.array([e1**2,e3**2])
    
    cA1, cA2 = np.linalg.cond(A1), np.linalg.cond(A2)
    logger.debug("Cond number A1: %.2e A2: %.2e"%(cA1, cA2))
    
    if cA1 < cA2:
        p2 = np.linalg.inv(A1) @ (np.array([d1**2, d2**2], ndmin=2)).T
    else:
        p2 = np.linalg.inv(A2) @ (np.array([d1**2, d3_2], ndmin=2)).T
        
    px, py = p2[0]**0.5, p2[1]**0.5
    logger.info("Calculated pixel size: %s"%((px, py),))
    
    XY_init = XY
    
    if psize_cor:
    
        logger.info('Pixel size correction')
        doagain = True
        while doagain:
            px_i = input("\nIdeal pixel x-size in mm: ")
            py_i = input("Ideal pixel y-size in mm: ") 
            try:
                px_i = float(px_i)
                py_i = float(py_i)
                
            except Exception as e:
                print(e)
                continue
            
            doagain = False
        
        C = np.array([data.shape[1]/2, data.shape[0]/2], ndmin=2)
        r = np.array([px/px_i, py/py_i], ndmin=2).T
        
        displ = displ*r + (XY-C)*(r-1)
        XY = (XY-C)*r + C
        
        px = px_i
        py = py_i
        
    
    logger.debug("e1 = %s"%(e1))
    logger.debug("e2 = %s"%(e2))
    
    # Splines
    logger.info("Compute the splines")
#    xx = np.linspace(0, data.shape[0], knots[0]+1)
#    yy = np.linspace(0, data.shape[1], knots[1]+1)
#    
#    xc = (xx[0:-1] + xx[1:])/2
#    yc = (yy[0:-1] + yy[1:])/2
#    
#    XX,YY = np.meshgrid(xc, yc)
    
    #plt.plot(XX, YY, '+g', mew=2, ms=10)
            
    # Create the splines
    
#    splx = ipol.LSQBivariateSpline(XY[:,1], XY[:,0], -displ[:,0], xc, yc, bbox=[0,data.shape[0],0,data.shape[1]])
#    sply = ipol.LSQBivariateSpline(XY[:,1], XY[:,0], -displ[:,1], xc, yc, bbox=[0,data.shape[0],0,data.shape[1]])
    
    splx = ipol.SmoothBivariateSpline(XY_init[:,0], XY_init[:,1], displ[:,0], bbox=[0,data.shape[0],0,data.shape[1]])
    sply = ipol.SmoothBivariateSpline(XY_init[:,0], XY_init[:,1], displ[:,1], bbox=[0,data.shape[0],0,data.shape[1]])

    # Display displacements
        
    logger.info("Display displacements maps and splines")
    
    
    x_im = np.arange(0,data.shape[1],5)
    y_im = np.arange(0,data.shape[0],5)
    Dx_spl = splx(x_im, y_im).T
    Dy_spl = sply(x_im, y_im).T
    
    M = max(np.max(np.abs(displ[:])), max(np.max(np.abs(Dx_spl[:])), np.max(np.abs(Dy_spl[:]))))
    
    cmap = mpl.cm.seismic
    norm = mpl.colors.Normalize(vmin=-M, vmax=M)    
    
    fig, ax = plt.subplots(2,2)
    ax[0,0].tricontourf(XY[:,0], XY[:,1], displ[:,0], cmap=cmap, norm=norm)
    ax[0,0].set_xlim([0, data.shape[1]])
    ax[0,0].set_ylim([0, data.shape[0]])
    ax[0,0].set_title("X-Displacement map")
    ax[0,1].tricontourf(XY[:,0], XY[:,1], displ[:,1], cmap=cmap, norm=norm)
    ax[0,1].set_xlim([0, data.shape[1]])
    ax[0,1].set_ylim([0, data.shape[0]])
    ax[0,1].set_title("Y-Displacement map")
    
    ax[1,0].pcolormesh(x_im, y_im, Dx_spl, cmap=cmap, norm=norm)
    ax[1,0].set_title("X-Displacement spline")
    ax[1,1].pcolormesh(x_im, y_im, Dy_spl, cmap=cmap, norm=norm)
    ax[1,1].set_title("Y-Displacement spline")
    
    fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)
    
    A = ax.ravel()
    for i in range(len(A)):
        A[i].axes.set_aspect('equal')
        
    
    answ = input("\nDisplay corrected initial image [yes/NO]?")
    
    if answ.lower() in ['o','y', 'oui', 'yes']:
        
        # Display corrected image
        logger.info("Correct initial image")
        plt.figure(3)
        
        plt.imshow(np.log(spline_correction(data, splx, sply)), origin='lower')
        plt.plot(XY[:,0], XY[:,1],'.b', label="Regular grid")
        plt.axis('scaled')
        
        plt.legend(loc='center left', bbox_to_anchor=(1.04,0.5))
    
    save_s = input('Save splines? [YES/no]')
    
    if save_s.lower() in ('', 'y', 'o', 'yes', 'oui'):
        namein = input('Filename [%s]'%output_file)
        
        if namein.strip() != '':
            output_file = namein.strip()
    
        logger.info("Save splines")        
        write_spline_file(output_file, splx, sply, px, py, data.shape)
    
    plt.show(block=True)
    
    
def main():
    
    import argparse
    
    parser = argparse.ArgumentParser(description="Get distorsion from file")
#    parser.add_argument('data_file', action='append', nargs='+', help='Data files')
    parser.add_argument('data_file', action='store', help='Data file')
    parser.add_argument('-R', dest='radius', action='store', default=15, type=int, help="Search radius")
#    parser.add_argument('-kNx', dest='kx', action='store', default=2, type=int, help="Number of knots along x")
#    parser.add_argument('-kNy', dest='ky', action='store', default=2, type=int, help="Number of knots along y")
    parser.add_argument('-v', dest='verbose', action='store_true', default=False, help="Verbose output")
    parser.add_argument('-q', dest='quiet', action='store_true', default=False, help="Quiet output")
    parser.add_argument('-P', '--no-pixsize-correction', dest='no_pixsize', action='store_true', default=False, help="Disable pixel size correction")
    parser.add_argument('-O', dest='outf', action='store', default=None, help="Output file")
    
    args = parser.parse_args()
    
    #logger.setLevel(logging.DEBUG if args.verbose else logging.WARNING if args.quiet else logging.INFO)
    # Set the log level of each package according to the defined verbosity
    loglevel = logging.INFO
    
    if args.verbose:
        loglevel = logging.DEBUG
    elif args.quiet:
        loglevel = logging.WARNING
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('spatdistcalib'):
            logging.getLogger(name).setLevel(loglevel)
    
    fn = args.outf
    if fn is None:
        fn = args.data_file
        if '.' in fn:
            fn = '.'.join(fn.split('.')[:-1])
            
        fn += '_splines'
        
        if os.path.isfile(fn+'.dat'):
            i=0
            while os.path.isfile('%s.%i.dat'%(fn,i)):
                i += 1
                
            fn += '.%i'%i
        fn += '.dat'
    
#    main(args.data_file, args.radius, (args.kx, args.ky), fn)
    process(args.data_file, args.radius, fn, not args.no_pixsize)