#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 16:23:59 2022

@author: opid02
"""


import sys
if sys.version_info < (3,6):
    sys.exit('Sorry, Python < 3.6 is not supported')


from setuptools import setup, find_packages

setup(name='spatdistcalib',
      version='0.01',
      python_requires='>3.6',
      description='ESRF ID02 Spatial Distortion Calibration',
      author='William Chevremont',
      author_email='william.chevremont@esrf.fr',
      install_requires=[
          'numpy',
          'scipy',
          'fabio',
          'matplotlib'
      ],
      packages=find_packages(include=['spatdistcalib', 'spatdistcalib.*']),
      entry_points={
                  'console_scripts': ['id02_spatdist=spatdistcalib.bin.spatdistcalib:main',
                                      'id02_img_to_edf=spatdistcalib.bin.spatdistcalib:main',
                                    ]
              },
#      package_data={'': ['det-config/*.ini']},
#      include_package_data=True,
    )
