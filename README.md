# SpatDistCalib

Spatial distorsion calibration in python, replacement for Fit2D.

## Installation

Using pip:

```bash
pip install https://gitlab.esrf.fr/chevremo/spatdistcalib/-/archive/main/spatdistcalib-main.zip
```

This is the recommended way.

## Usage

This program compute the correction splines for 2D detector calibration. It is
saved in .dat file, usable by [spd program](https://www.esrf.fr/home/UsersAndScience/Experiments/CBS/ID02/available_software/saxs-program-package.html)
for distorsion correction.

First, the program ask for base vector. You need to click first on the origin (0,0),
then on (1,0) then on (0,1) in local base vector. This base do not need to be orthogonal,
so that hexagonal pattern can be used.

Then, the program ask for the real-space distances of the vector base, to determine 
pixel size. The measured displacement are then showed in a new figure with
the corresponding splines.

Finally, you can display the calibration image with the correction applied, and save the splines.

```bash
$ id02_spatdist -h
usage: id02_spatdist [-h] [-R RADIUS] [-v] [-q] [-P] [-O OUTF] data_file

Get distorsion from file

positional arguments:
  data_file             Data file

optional arguments:
  -h, --help            show this help message and exit
  -R RADIUS             Search radius
  -v                    Verbose output
  -q                    Quiet output
  -P, --no-pixsize-correction
                        Disable pixel size correction
  -O OUTF               Output file

$ id02_spatdist tests/contracted_x.edf -R10
INFO spatdistcalib.bin.spatdistcalib : Processing file tests/contracted_x.edf
Select origin and 2 points along the main axes from the origin. Right click remove the last point.
INFO spatdistcalib.bin.spatdistcalib : Probing along the main directions
INFO spatdistcalib.bin.spatdistcalib : Generating regular grid
INFO spatdistcalib.bin.spatdistcalib : Find each individual peaks
INFO spatdistcalib.bin.spatdistcalib : Found 100 peaks
INFO spatdistcalib.bin.spatdistcalib : Found 200 peaks
INFO spatdistcalib.bin.spatdistcalib : Found 300 peaks
INFO spatdistcalib.bin.spatdistcalib : Found 400 peaks
INFO spatdistcalib.bin.spatdistcalib : Found 500 peaks
INFO spatdistcalib.bin.spatdistcalib : Removing displacement anomalities
INFO spatdistcalib.bin.spatdistcalib : 141 anomalities found
INFO spatdistcalib.bin.spatdistcalib : Calculating pixel size

Real space distance in e1 direction in mm: 1
Real space distance in e2 direction in mm: 1
Angle between main directions [65.00]: 60
INFO spatdistcalib.bin.spatdistcalib : Calculated pixel size: (array([0.05237319]), array([0.04071716]))
INFO spatdistcalib.bin.spatdistcalib : Pixel size correction

Ideal pixel x-size in mm: 0.04
Ideal pixel y-size in mm: 0.04
INFO spatdistcalib.bin.spatdistcalib : Compute the splines
INFO spatdistcalib.bin.spatdistcalib : Display displacements maps and splines

Display corrected initial image [yes/NO]?y
INFO spatdistcalib.bin.spatdistcalib : Correct initial image
INFO spatdistcalib.utils.utils : Generate displacement matrix from spline
INFO spatdistcalib.utils.utils : Transformation matrix calculation
INFO spatdistcalib.utils.utils : ... Masking
INFO spatdistcalib.utils.utils : ... Indexing
INFO spatdistcalib.utils.utils : ... Summation
INFO spatdistcalib.utils.utils : ... Normalization
INFO spatdistcalib.utils.utils : Image correction
Save splines? [YES/no]y
Filename [tests/contracted_x_splines.dat]
INFO spatdistcalib.bin.spatdistcalib : Save splines

$ spd distortion_file=tests/contracted_x_splines.dat do_distortion=1 tests/contracted_x.edf
spd_1.9.1_linux64 compiled Mar 21 2022, 16:08:33
edfio : V3.4.2  Peter Boesecke 2022-03-10 (E2.412_linux64)
bslio : V0.73 Peter Boesecke 2018-04-06
h5io : V1.2.0 Peter Boesecke 2021-04-08
h5sx-1.4.2: Peter Boesecke 2022-03-10
Correcting: tests/contracted_x.edf
        --> tests/contracted_x_cor.edf
Max Distortion: x = 61.151241 y = 5.243320
Min Distortion: x = 0.000153 y = 0.000061
Max Pixel size: x = 3 y = 3
Max SRC Pixel in Target : 4 at [2,1]
Parts Count total: 925809
Going to produce the correction program
...................................................
Delete untouched pixels
Saving corrected image 0 to file tests/contracted_x_cor.edf
```